import {applyMiddleware, combineReducers, createStore} from "redux";
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";
import thunkMiddleware from "redux-thunk";
import getArtists from "./reducers/getArtists";
import getAlbums from "./reducers/getAlbums";
import getTracks from "./reducers/getTracks";
import createUser from "./reducers/createUser";
import getUser from "./reducers/getUser";
import getTrackHistory from "./reducers/getTrackHistory";
import addArtist from "./reducers/addArtist";
import addAlbum from "./reducers/addAlbum";
import addTrack from "./reducers/addTrack";
import unpublished from "./reducers/unpublished";

const rootReducers = combineReducers({
    getArtists: getArtists,
    getAlbums: getAlbums,
    getTracks: getTracks,
    createUser: createUser,
    getUser: getUser,
    getTrackHistory: getTrackHistory,
    addArtist: addArtist,
    addAlbum: addAlbum,
    addTrack: addTrack,
    unpublished: unpublished,
});

const persistedState = loadFromLocalStorage();

const store = createStore(rootReducers, persistedState, applyMiddleware(thunkMiddleware));

store.subscribe(() => {
    saveToLocalStorage({
        getUser: {
            user: store.getState().getUser.user
        }
    });
});

export default store;