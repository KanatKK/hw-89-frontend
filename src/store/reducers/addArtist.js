import {ADD_ARTISTS_DESCRIPTION, ADD_ARTISTS_IMAGE, ADD_ARTISTS_IMAGE_NAME, ADD_ARTISTS_NAME} from "../actionTypes";

const initialState = {
    name: "",
    description: "",
    image: null,
    imageName: "Choose and image.",
    published: false,
};

const addArtist = (state = initialState, action) => {
    switch (action.type) {
        case ADD_ARTISTS_NAME:
            return {...state, name: action.value};
        case ADD_ARTISTS_DESCRIPTION:
            return {...state, description: action.value};
        case ADD_ARTISTS_IMAGE:
            return {...state, image: action.value};
        case ADD_ARTISTS_IMAGE_NAME:
            return {...state, imageName: action.value};
        default:
            return state;
    }
};

export default addArtist;