import {GET_UNPUBLISHED_ALBUMS, GET_UNPUBLISHED_ARTISTS, GET_UNPUBLISHED_TRACKS} from "../actionTypes";

const initialState = {
    artists: null,
    albums: null,
    tracks: null,
};

const unpublished = (state = initialState, action) => {
    switch (action.type) {
        case GET_UNPUBLISHED_TRACKS:
            return {...state, tracks: action.value};
        case GET_UNPUBLISHED_ARTISTS:
            return {...state, artists: action.value};
        case GET_UNPUBLISHED_ALBUMS:
            return {...state, albums: action.value};
        default:
            return state;
    }
};

export default unpublished;