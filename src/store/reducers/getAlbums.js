import {GET_ALBUMS} from "../actionTypes";

const initialState = {
    albums: null,
};

const getAlbums = (state = initialState, action) => {
    switch (action.type) {
        case GET_ALBUMS:
            return {...state, albums: action.value};
        default:
            return state;
    }
};

export default getAlbums;