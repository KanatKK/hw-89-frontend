import {GET_TRACK_HISTORY} from "../actionTypes";

const initialState = {
    trackHistory: null,
};

const getTrackHistory = (state = initialState, action) => {
    switch (action.type) {
        case GET_TRACK_HISTORY:
            return {...state, trackHistory: action.value};
        default:
            return state;
    }
};

export default getTrackHistory;