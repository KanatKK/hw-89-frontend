import {
    ADD_ALBUMS_ARTIST,
    ADD_ALBUMS_IMAGE,
    ADD_ALBUMS_IMAGE_NAME,
    ADD_ALBUMS_NAME,
    ADD_ALBUMS_YEAR
} from "../actionTypes";

const initialState = {
    name: "",
    artist: "",
    year: "",
    image: null,
    imageName: "Choose an image.",
    published: false,
};

const addAlbum = (state = initialState, action) => {
    switch (action.type) {
        case ADD_ALBUMS_NAME:
            return {...state, name: action.value};
        case ADD_ALBUMS_YEAR:
            return {...state, year: action.value};
        case ADD_ALBUMS_ARTIST:
            return {...state, artist: action.value};
        case ADD_ALBUMS_IMAGE:
            return {...state, image: action.value};
        case ADD_ALBUMS_IMAGE_NAME:
            return {...state, imageName: action.value};
        default:
            return state;
    }
};

export default addAlbum;