import {GET_TRACKS} from "../actionTypes";

const initialState = {
    tracks: null,
};

const getTracks = (state = initialState, action) => {
    switch (action.type) {
        case GET_TRACKS:
            return {...state, tracks: action.value};
        default:
            return state;
    }
};

export default getTracks;