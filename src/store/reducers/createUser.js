import {ADD_DISPLAY_NAME, ADD_IMAGE_LINK, ADD_PASSWORD, ADD_USERNAME} from "../actionTypes";

const initialState = {
    username: "",
    password: "",
    displayName: "",
    imageLink: "",
};

const createUser = (state = initialState, action) => {
    switch (action.type) {
        case ADD_IMAGE_LINK:
            return {...state, imageLink: action.value};
        case ADD_DISPLAY_NAME:
            return {...state, displayName: action.value};
        case ADD_PASSWORD:
            return {...state, password: action.value};
        case ADD_USERNAME:
            return {...state, username: action.value};
        default:
            return state;
    }
};

export default createUser;