import {ADD_TRACK_ALBUM, ADD_TRACK_DURATION, ADD_TRACK_NAME, ADD_TRACK_NUMBER} from "../actionTypes";

const initialState = {
    name: "",
    duration: "",
    number: "",
    album: "",
    published: false
};

const addTrack = (state = initialState, action) => {
    switch (action.type) {
        case ADD_TRACK_NAME:
            return {...state, name: action.value};
        case ADD_TRACK_DURATION:
            return {...state, duration: action.value};
        case ADD_TRACK_NUMBER:
            return {...state, number: action.value};
        case ADD_TRACK_ALBUM:
            return {...state, album: action.value};
        default:
            return state;
    }
};

export default addTrack;