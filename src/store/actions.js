import axios from "axios";
import {
    ADD_ALBUMS_ARTIST, ADD_ALBUMS_IMAGE, ADD_ALBUMS_IMAGE_NAME,
    ADD_ALBUMS_NAME, ADD_ALBUMS_YEAR,
    ADD_ARTISTS_DESCRIPTION, ADD_ARTISTS_IMAGE, ADD_ARTISTS_IMAGE_NAME,
    ADD_ARTISTS_NAME, ADD_DISPLAY_NAME, ADD_IMAGE_LINK,
    ADD_PASSWORD, ADD_TRACK_ALBUM, ADD_TRACK_DURATION, ADD_TRACK_NAME, ADD_TRACK_NUMBER,
    ADD_USERNAME,
    GET_ALBUMS,
    GET_ARTISTS,
    GET_TRACK_HISTORY,
    GET_TRACKS, GET_UNPUBLISHED_ALBUMS, GET_UNPUBLISHED_ARTISTS, GET_UNPUBLISHED_TRACKS,
    GET_USER
} from "./actionTypes";
import {apiUrl} from "../constants";

const getArtistsList = value => {
    return {type: GET_ARTISTS, value};
};

const getAlbumsList = value => {
    return {type: GET_ALBUMS, value};
};

const getTrackList = value => {
    return {type: GET_TRACKS, value};
};

export const fetchUser = value => {
    return {type: GET_USER, value};
};

export const getTrackHistory = value => {
    return {type: GET_TRACK_HISTORY, value};
};

export const addUserName = value => {
    return {type: ADD_USERNAME, value};
};

export const addPassword = value => {
    return {type: ADD_PASSWORD, value};
};

export const addDisplayName = value => {
    return {type: ADD_DISPLAY_NAME, value};
};

export const addImageLink = value => {
    return {type: ADD_IMAGE_LINK, value};
};

export const addArtistName = value => {
    return {type: ADD_ARTISTS_NAME, value};
};

export const addArtistDescription = value => {
    return {type: ADD_ARTISTS_DESCRIPTION, value};
};

export const addArtistImage = value => {
    return {type: ADD_ARTISTS_IMAGE, value};
};

export const addArtistImageName = value => {
    return {type: ADD_ARTISTS_IMAGE_NAME, value};
};

export const addAlbumName = value => {
    return {type: ADD_ALBUMS_NAME, value};
};

export const addAlbumYear = value => {
    return {type: ADD_ALBUMS_YEAR, value};
};

export const addAlbumArtist = value => {
    return {type: ADD_ALBUMS_ARTIST, value};
};

export const addAlbumImage = value => {
    return {type: ADD_ALBUMS_IMAGE, value};
};

export const addAlbumImageName = value => {
    return {type: ADD_ALBUMS_IMAGE_NAME, value};
};

export const addTrackName = value => {
    return {type: ADD_TRACK_NAME, value};
};

export const addTrackDuration = value => {
    return {type: ADD_TRACK_DURATION, value};
};

export const addTrackNumber = value => {
    return {type: ADD_TRACK_NUMBER, value};
};

export const addTrackAlbum = value => {
    return {type: ADD_TRACK_ALBUM, value};
};

export const getUnpublishedTracks = value => {
    return {type: GET_UNPUBLISHED_TRACKS, value};
};

export const getUnpublishedAlbums = value => {
    return {type: GET_UNPUBLISHED_ALBUMS, value};
};

export const getUnpublishedArtists = value => {
    return {type: GET_UNPUBLISHED_ARTISTS, value};
};

export const deleteArtist = async (headers, id) => {
    try {
        await axios.delete(apiUrl + '/artists/' + id, {headers});
    } catch (e) {
        console.log(e);
    }
};

export const deleteAlbum = async (headers, id) => {
    try {
        await axios.delete(apiUrl + '/albums/' + id, {headers});
    } catch (e) {
        console.log(e);
    }
};

export const deleteTrack = async (headers, id) => {
    try {
        await axios.delete(apiUrl + '/tracks/' + id, {headers});
    } catch (e) {
        console.log(e);
    }
};

export const publishTrack = async (id, headers) => {
    try {
        await axios.post(apiUrl + '/tracks/published/' + id, id, {headers});
    } catch (e) {
        console.log(e);
    }
};

export const publishArtist = async (id, headers) => {
    try {
        await axios.post(apiUrl + '/artists/published/' + id, id, {headers});
    } catch (e) {
        console.log(e);
    }
};

export const publishAlbum = async (id, headers) => {
    try {
        await axios.post(apiUrl + '/albums/published/' + id, id, {headers});
    } catch (e) {
        console.log(e);
    }
};

export const fetchUnpublishedArtists = (headers) => {
    return async dispatch => {
        try {
            const response = await axios.get(apiUrl + '/artists/unpublished', {headers});
            dispatch(getUnpublishedArtists(response.data));
        } catch (e) {
            console.log(e);
        }
    }
};

export const fetchUnpublishedAlbums = (headers) => {
    return async dispatch => {
        try {
            const response = await axios.get(apiUrl + '/albums/unpublished', {headers});
            dispatch(getUnpublishedAlbums(response.data));
        } catch (e) {
            console.log(e);
        }
    }
};

export const fetchUnpublishedTracks = (headers) => {
    return async dispatch => {
        try {
            const response = await axios.get(apiUrl + '/tracks/unpublished', {headers});
            dispatch(getUnpublishedTracks(response.data));
        } catch (e) {
            console.log(e);
        }
    }
};

export const addNewTrack = async (headers, track) => {
    try {
        await axios.post(apiUrl + '/tracks', track, {headers});
    } catch (e) {
        console.log(e);
    }
};

export const addNewAlbum = async (headers, album) => {
    try {
        await axios.post(apiUrl + '/albums', album, {headers});
    } catch (e) {
        console.log(e);
    }
};

export const addNewArtist = async (headers, artist) => {
    try{
        await axios.post(apiUrl + '/artists', artist, {headers});
    } catch (e) {
        console.log(e);
    }
};

export const fetchArtists = () => {
    return async dispatch => {
        try {
            const response = await axios.get(apiUrl + '/artists');
            dispatch(getArtistsList(response.data));
        } catch (e) {
            console.log(e);
        }
    };
};

export const fetchAlbums = (artistId) => {
    return async dispatch => {
        if (artistId) {
            try {
                const response = await axios.get(apiUrl + '/albums?artist=' + artistId);
                dispatch(getAlbumsList(response.data));
            } catch (e) {
                console.log(e);
            }
        } else {
            try {
                const response = await axios.get(apiUrl + '/albums');
                dispatch(getAlbumsList(response.data));
            } catch (e) {
                console.log(e);
            }
        }
    };
};

export const fetchTracks = (albumId) => {
    return async dispatch => {
        try {
            const response = await axios.get(apiUrl + '/tracks?album=' + albumId);
            dispatch(getTrackList(response.data));
        } catch (e) {
            console.log(e);
        }
    };
};

export const createUser = (data) => {
    return async dispatch => {
        try {
            const response = await axios.post(apiUrl + '/users', data);
            dispatch(fetchUser(response.data))
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(fetchUser(e.response.data));
            }
        }
    };
};

export const registerUser = (data) => {
    return async dispatch => {
        try {
            const response = await axios.post(apiUrl + '/users/sessions', data);
            dispatch(fetchUser(response.data));
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(fetchUser(e.response.data));
            }
        }
    };
};

export const fetchTrackHistory = (user) => {
    return async dispatch => {
        try {
            const response = await axios.get(apiUrl + '/track_history/' + user);
            dispatch(getTrackHistory(response.data));
        } catch (e) {
            console.log(e);
        }
    };
};

export const addTrackHistory = async (track, headers) => {
    try {
        await axios.post(apiUrl + '/track_history', track, {headers: headers});
    } catch (e) {
        console.log(e);
    }
};

export const logOutUser = () => {
    return async (dispatch, getState) => {
        const token = getState().getUser.user.token;
        const headers = {"Authorization": token};
        await axios.delete(apiUrl + '/users/sessions', {headers});
        dispatch(fetchUser(null));
    }
};

export const facebookLogin = data => {
    return async dispatch => {
        try {
            const response = await axios.post(apiUrl + '/users/facebookLogin', data);
            await dispatch(fetchUser(response.data));
        } catch (e) {
            console.log(e);
        }
    };
};