import React from 'react';
import './App.css';
import {Route, Switch, BrowserRouter} from 'react-router-dom';
import Artists from "../../components/Artists/Artists";
import Albums from "../../components/Albums/Albums";
import Tracks from "../../components/Tracks/Tracks";
import SignUp from "../../components/Registration/SignUp";
import LogIn from "../../components/Registration/LogIn";
import TrackHistory from "../../components/TrackHistory/TrackHistory";
import CreateArtist from "../../components/Create/CreateArtist";
import CreateAlbum from "../../components/Create/CreateAlbum";
import CreateTrack from "../../components/Create/CreateTrack";
import Unpublished from "../../components/Unpublished/Unpublished";

const App = () => {
  return (
      <BrowserRouter>
        <Switch>
            <Route path="/sign_in" exact component={SignUp}/>
            <Route path="/login" exact component={LogIn}/>
            <Route path="/" exact component={Artists}/>
            <Route path="/albums/:id" exact component={Albums}/>
            <Route path="/tracks/:id" exact component={Tracks}/>
            <Route path="/track_history/:user" exact component={TrackHistory}/>
            <Route path="/addArtist" exact component={CreateArtist}/>
            <Route path="/addAlbum" exact component={CreateAlbum}/>
            <Route path="/addTrack" exact component={CreateTrack}/>
            <Route path="/unpublished" exact component={Unpublished}/>
        </Switch>
      </BrowserRouter>
  );
};

export default App;
