import React from 'react';
import {NavLink} from "react-router-dom";
import {useSelector} from "react-redux";

const CreateBlock = () => {
    const user = useSelector(state => state.getUser.user);
    if (user && user.username !== undefined) {
        return (
            <div className="addBlock">
                <NavLink
                    style={{
                        color: 'black',
                        textDecoration: 'underline',
                        cursor: 'pointer',
                        marginLeft: 15,
                        marginRight: 15,
                    }}
                    to="/addArtist"
                >Create Artist</NavLink>
                <NavLink
                    style={{
                        color: 'black',
                        textDecoration: 'underline',
                        cursor: 'pointer',
                        marginLeft: 15,
                        marginRight: 15,
                    }}
                    to="/addAlbum"
                >Create Album</NavLink>
                <NavLink
                    style={{
                        color: 'black',
                        textDecoration: 'underline',
                        cursor: 'pointer',
                        marginLeft: 15,
                    }}
                    to="/addTrack"
                >Create Track</NavLink>
            </div>
        );
    } else {
        return null;
    }
};

export default CreateBlock;