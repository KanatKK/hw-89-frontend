import React from 'react';
import RegistrationBlock from "../Registration/RegistrationBlock";
import {useDispatch, useSelector} from "react-redux";
import {
    addArtistDescription,
    addArtistImage,
    addArtistImageName,
    addArtistName,
    addNewArtist
} from "../../store/actions";

const CreateArtist = props => {
    const dispatch = useDispatch();
    const name = useSelector(state => state.addArtist.name);
    const description = useSelector(state => state.addArtist.description);
    const imageName = useSelector(state => state.addArtist.imageName);
    const artistData = useSelector(state => state.addArtist);
    const user = useSelector(state => state.getUser.user);

    const addNameHandler = event => {
        dispatch(addArtistName(event.target.value));
    };

    const addDescriptionHandler = event => {
        dispatch(addArtistDescription(event.target.value));
    };

    const addImageHandler = event => {
        if (event.target.files[0] !== undefined) {
            dispatch(addArtistImageName(event.target.files[0].name));
            dispatch(addArtistImage(event.target.files[0]));
        } else {
            dispatch(addArtistImage(null));
            dispatch(addArtistImageName("Choose an image..."));
        }
    };

    const send = async (event) => {
        event.preventDefault();
        const formData = new FormData();
        Object.keys(artistData).forEach(key => {
            if (key !== 'imageName') {
                formData.append(key, artistData[key]);
            }
        });
        const headers = {
            'Authentication': user.token,
        };
        await addNewArtist(headers, formData);
        dispatch(addArtistName(""));
        dispatch(addArtistDescription(""));
        dispatch(addArtistImage(null));
        dispatch(addArtistImageName("Choose an image..."));
        props.history.push("/");
    };

    return (
        <div className="container">
            <header>
                <h2>Create Artist</h2>
                <RegistrationBlock/>
            </header>
            <div className="content">
                <input
                    type="text" value={name} onChange={addNameHandler}
                    className="createItemField" placeholder="Name" name="artistName"
                />
                <input
                    type="text" value={description} onChange={addDescriptionHandler}
                    className="createItemField" placeholder="Description" name="artistDescription"
                />
                <label className="inputFileLabel" htmlFor="inputFile">{imageName}</label>
                <input
                    type="file" className="chooseImage" onChange={addImageHandler}
                    id="inputFile" accept=".jpg, .jpeg, .png"
                />
                <button className="addButton" name="createArtist" onClick={send}>Create</button>
            </div>
        </div>
    );
};

export default CreateArtist;