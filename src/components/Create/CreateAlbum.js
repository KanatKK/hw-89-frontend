import React, {useEffect} from 'react';
import RegistrationBlock from "../Registration/RegistrationBlock";
import {useDispatch, useSelector} from "react-redux";
import {
    addAlbumArtist,
    addAlbumName,
    addAlbumYear,
    addAlbumImage,
    addAlbumImageName,
    fetchArtists, addNewAlbum
} from "../../store/actions";

const CreateAlbum = props => {
    const dispatch = useDispatch();
    const artists = useSelector(state => state.getArtists.artists);
    const user = useSelector(state => state.getUser.user);
    const name = useSelector(state => state.addAlbum.name);
    const year = useSelector(state => state.addAlbum.year);
    const imageName = useSelector(state => state.addAlbum.imageName);
    const albumData = useSelector(state => state.addAlbum);

    useEffect(() => {
        dispatch(fetchArtists());
    }, [dispatch]);

    const addAlbumNameHandler = event => {
        dispatch(addAlbumName(event.target.value));
    };

    const addAlbumYearHandler = event => {
        dispatch(addAlbumYear(event.target.value));
    };

    const addAlbumArtistHandler = event => {
        dispatch(addAlbumArtist(event.target.value));
    };

    const addImageHandler = event => {
        if (event.target.files[0] !== undefined) {
            dispatch(addAlbumImageName(event.target.files[0].name));
            dispatch(addAlbumImage(event.target.files[0]));
        } else {
            dispatch(addAlbumImage(null));
            dispatch(addAlbumImageName("Choose an image..."));
        }
    };

    const send = async (event) => {
        event.preventDefault();
        const formData = new FormData();
        Object.keys(albumData).forEach(key => {
            if (key !== 'imageName') {
                formData.append(key, albumData[key]);
            }
        });
        const headers = {
            'Authentication': user.token,
        };
        await addNewAlbum(headers,formData);
        dispatch(addAlbumName(""));
        dispatch(addAlbumYear(""));
        dispatch(addAlbumArtist(""));
        dispatch(addAlbumImageName("Choose an image..."));
        dispatch(addAlbumImage(null));
        props.history.push("/");
    };

    if (artists !== null) {
        const options = artists.map((artist, index) => {
            return (
                <option key={index} name={artist.name} value={artist._id}>{artist.name}</option>
            );
        });
        return (
            <div className="container">
                <header>
                    <h2>Create Album</h2>
                    <RegistrationBlock/>
                </header>
                <div className="content">
                    <input
                        type="text" value={name} onChange={addAlbumNameHandler}
                        className="createItemField" placeholder="Album Name" name="albumName"
                    />
                    <input
                        type="text" value={year} onChange={addAlbumYearHandler}
                        className="createItemField" placeholder="Year" name="albumYear"
                    />
                    <label className="inputFileLabel" htmlFor="inputFile">{imageName}</label>
                    <input
                        type="file" className="chooseImage" onChange={addImageHandler}
                        id="inputFile" accept=".jpg, .jpeg, .png"
                    />
                    <div className="categoriesBloc">
                        <label>
                            Choose an artist:
                            <select
                                onChange={addAlbumArtistHandler} name="category"
                                className="categoryChanger" placeholder="select"
                            >
                                {options}
                            </select>
                        </label>
                    </div>
                    <button className="addButton" name="createAlbum" onClick={send}>Create</button>
                </div>
            </div>
        );
    } else {
        return null;
    }
};

export default CreateAlbum;