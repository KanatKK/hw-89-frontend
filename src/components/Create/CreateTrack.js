import React, {useEffect} from 'react';
import RegistrationBlock from "../Registration/RegistrationBlock";
import {useDispatch, useSelector} from "react-redux";
import {
    addNewTrack,
    addTrackAlbum,
    addTrackDuration,
    addTrackName,
    addTrackNumber,
    fetchAlbums
} from "../../store/actions";

const CreateTrack = props => {
    const dispatch = useDispatch()
    const albums = useSelector(state => state.getAlbums.albums);
    const name = useSelector(state => state.addTrack.name);
    const number = useSelector(state => state.addTrack.number);
    const duration = useSelector(state => state.addTrack.duration);
    const trackData = useSelector(state => state.addTrack);
    const user = useSelector(state => state.getUser.user);

    useEffect(() => {
        dispatch(fetchAlbums());
    }, [dispatch]);

    const addTrackNameHandler = event => {
        dispatch(addTrackName(event.target.value));
    };

    const addTrackNumberHandler = event => {
        dispatch(addTrackNumber(event.target.value));
    };

    const addTrackDurationHandler = event => {
        dispatch(addTrackDuration(event.target.value));
    };

    const addTrackAlbumHandler = event => {
        dispatch(addTrackAlbum(event.target.value));
    };

    const send = async () => {
        const headers = {
            'Authentication': user.token,
        };
        await addNewTrack(headers, trackData);
        props.history.push("/");
    };

    if (albums !== null) {
        const options = albums.map((album, index) => {
            return (
                <option key={index} value={album._id}>{album.name}</option>
            )
        });

        return (
            <div className="container">
                <header>
                    <h2>Add Track</h2>
                    <RegistrationBlock/>
                </header>
                <div className="content">
                    <input
                        type="text" value={name} onChange={addTrackNameHandler}
                        className="createItemField" placeholder="Track Name" name="trackName"
                    />
                    <input
                        type="text" value={duration} onChange={addTrackDurationHandler}
                        className="createItemField" placeholder="Track Duration" name="trackDuration"
                    />
                    <input
                        type="text" value={number} onChange={addTrackNumberHandler}
                        className="createItemField" placeholder="Track Number" name="trackNumber"
                    />
                    <div className="categoriesBloc">
                        <label>
                            Choose an album:
                            <select
                                name="category" onChange={addTrackAlbumHandler}
                                className="categoryChanger" placeholder="select"
                            >
                                {options}
                            </select>
                        </label>
                    </div>
                    <button className="addButton" name="createTrack" onClick={send}>Create</button>
                </div>
            </div>
        );
    } else {
        return null;
    }
};

export default CreateTrack;