import React from 'react';
import {useSelector} from "react-redux";
import {addTrackHistory, deleteTrack} from "../../store/actions";

const TrackList = props => {
    const user = useSelector(state => state.getUser.user);

    const deleteTrackHandler = async (event) => {
        const token = user.token;
        const headers = {"Authentication": token};
        await deleteTrack(headers, event.target.value);
    };

    const addTrackHistoryHandler = async event => {
        if (user !== null && user.username !== undefined) {
            const headers = {
                'Authentication': user.token,
            }
            await addTrackHistory({track: event.target.id, artist: props.artist}, headers);
        }
    };
    return (
        <div className="track">
            <div className="trackNumber">
                {props.number}
            </div>
            <div className="trackInfo">
                <h4>{props.name}</h4>
                <p>{props.duration}</p>
            </div>
                <button className="player" name="player" id={props.id} onClick={addTrackHistoryHandler}>
                    ►
                </button>
            {
                user && user.role==="admin" &&
                <button
                    value={props.id} onClick={deleteTrackHandler}
                    className="deleteBtn"
                >X</button>
            }
        </div>
    );
};

export default TrackList;