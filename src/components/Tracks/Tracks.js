import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchArtists, fetchTracks} from "../../store/actions";
import TrackList from "./TrackList";
import RegistrationBlock from "../Registration/RegistrationBlock";
import CreateBlock from "../Create/CreateBlock";
import {port} from "../../constants";

const Tracks = props => {
    const dispatch = useDispatch();
    const artists = useSelector(state => state.getArtists.artists);
    const tracks = useSelector(state => state.getTracks.tracks);

    useEffect(() => {
        dispatch(fetchArtists());
        dispatch(fetchTracks(props.match.params.id));
    }, [dispatch, props.match.params.id]);

    useEffect(() => {
        const interval = setInterval(async () => {
            dispatch(fetchArtists());
            dispatch(fetchTracks(props.match.params.id));
        }, 2000);
        return () => clearInterval(interval);
    }, [dispatch, props.match.params.id]);

    if (tracks !== null && tracks[0] !== undefined) {
        let artistName = null
        if (artists !== null) {
            Object.keys(artists).forEach(key => {
                if (artists[key]._id === tracks[0].album.artist) {
                    artistName = artists[key].name;
                }
            });
        }
        const trackList = tracks.map((track, index) => {
            return (
                <TrackList
                    key={index} artist={artistName} number={track.number}
                    name={track.name} duration={track.duration} id={track._id}
                />
            );
        });
        return (
            <div className="container">
                <header>
                    <h2>Tracks</h2>
                    <RegistrationBlock/>
                </header>
                <CreateBlock/>
                <div className="content">
                    <h2>{artistName}</h2>
                    <h3>{tracks[0].album.name && tracks[0].album.name}</h3>
                    <div className="albumImageForTracks">
                        <img
                            src={'http://localhost:'+ port +'/uploads/' + tracks[0].album.image}
                            alt="" className="albumImageForTracks"
                        />
                    </div>
                    <div className="tracks">
                        {trackList}
                    </div>
                </div>
            </div>
        );
    } else {
        return (
            <div className="container">
                <header>
                    <h2>Tracks</h2>
                    <RegistrationBlock/>
                </header>
                <div className="content">
                    <h2>This album is empty, login or sign up and add some tracks!</h2>
                </div>
            </div>
        );
    }
};

export default Tracks;