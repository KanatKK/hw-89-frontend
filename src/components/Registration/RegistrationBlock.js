import React from 'react';
import {NavLink} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {logOutUser} from "../../store/actions";

const RegistrationBlock = () => {
    const user = useSelector(state => state.getUser.user);
    const dispatch = useDispatch();

    const logOut = () => {
        dispatch(logOutUser());
    };

    if (user && user.username !== undefined) {
        if (user.role === "admin") {
            return (
                <span className="nav">
                    <span className="unpublished">
                    <NavLink
                        style={{
                            color: 'black',
                            textDecoration: 'underline',
                            cursor: 'pointer'
                        }}
                        to={'/unpublished'}
                    >
                        Unpublished
                    </NavLink>
                </span>
                <span className="trackHistory">
                    <NavLink
                        style={{
                            color: 'black',
                            textDecoration: 'underline',
                            cursor: 'pointer',
                            marginLeft: 20,
                        }}
                        to={'/track_history/' + user.username}
                    >
                        Track History
                    </NavLink>
                </span>
                <span className="name" onClick={logOut}>
                    <NavLink
                        style={{
                            color: 'black',
                            textDecoration: 'underline',
                            cursor: 'pointer'
                        }}
                        to="/"
                    >{user.displayName}</NavLink>
                </span>
                    <img className="userImage" src={user.imageLink} alt=""/>
            </span>
            );
        } else {
            return (
                <span className="nav">
                <span className="trackHistory">
                    <NavLink
                        style={{
                            color: 'black',
                            textDecoration: 'underline',
                            cursor: 'pointer'
                        }}
                        to={'/track_history/' + user.username}
                    >
                        Track History
                    </NavLink>
                </span>
                <span className="name" onClick={logOut}>
                    <NavLink
                        style={{
                            color: 'black',
                            textDecoration: 'underline',
                            cursor: 'pointer'
                        }}
                        to="/"
                    >{user.displayName}</NavLink>
                </span>
                    <img className="userImage" src={user.imageLink} alt=""/>
            </span>
            );
        }
    } else {
        return (
            <span className="nav">
            <span className="signIn">
                <NavLink
                    style={{
                        color: 'black',
                        textDecoration: 'underline',
                        cursor: 'pointer'
                    }}
                    to="/sign_in"
                >Sign In</NavLink>
            </span>
            |
            <span className="login">
                <NavLink
                    style={{
                        color: 'black',
                        textDecoration: 'underline',
                        cursor: 'pointer'
                    }}
                    to="/login"
                >Login</NavLink>
            </span>
        </span>
        );
    }
};

export default RegistrationBlock;