import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {addDisplayName, addImageLink, addPassword, addUserName, createUser} from "../../store/actions";
import {NavLink} from "react-router-dom";
import FacebookLogin from "../FacebookLogin/FacebookLogin";

const SignUp = props => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.getUser.user);
    const userName = useSelector(state => state.createUser.username);
    const password = useSelector(state => state.createUser.password);
    const displayName = useSelector(state => state.createUser.displayName);
    const imageLink = useSelector(state => state.createUser.imageLink);
    const userData = useSelector(state => state.createUser);

    const toLogIn = () => {
        props.history.push("/login");
    };

    const addUserNameHandler = event => {
        dispatch(addUserName(event.target.value));
    };

    const addPasswordHandler = event => {
        dispatch(addPassword(event.target.value));
    };

    const addDisplayNameHandler = event => {
        dispatch(addDisplayName(event.target.value));
    };

    const addImageLinkHandler = event => {
        dispatch(addImageLink(event.target.value));
    };

    const createUserHandler = () => {
        dispatch(createUser(userData));
        dispatch(addUserName(""));
        dispatch(addPassword(""));
    };

    if (user !== null && user.username) {
        props.history.push("/");
    }

    return (
        <div className="container">
            <header>
                <h2>Registration</h2>
                <span className="nav">
                    <NavLink
                        style={{
                            color: 'black',
                            textDecoration: 'underline',
                            cursor: 'pointer'
                        }}
                        to={'/'}
                    >
                        Artists
                    </NavLink>
                </span>
            </header>
            <div className="registration">
                <input
                    type="text" value={userName}
                    className="userName" placeholder="User Name"
                    onChange={addUserNameHandler} name="username"
                />
                <input
                    type="text" value={displayName}
                    className="displayName" placeholder="Display Name"
                    onChange={addDisplayNameHandler} name="displayName"
                />
                <input
                    type="password" value={password}
                    className="password" placeholder="Password"
                    onChange={addPasswordHandler} name="password"
                />
                <input
                    type="text" value={imageLink}
                    className="imageLink" placeholder="Image Link"
                    onChange={addImageLinkHandler} name="imageLink"
                />
                <button
                    onClick={createUserHandler} type="button"
                    name="regBtn" className="regButton"
                >Create new account</button>
                <FacebookLogin/>
                {user && <span className="error">{user.error}</span>}
                <p onClick={toLogIn}>Already have an account? Login!</p>
            </div>
        </div>
    );
};

export default SignUp;