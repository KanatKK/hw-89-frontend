import React from 'react';
import {NavLink} from "react-router-dom";
import {useSelector} from "react-redux";
import {deleteAlbum} from "../../store/actions";
import {port} from "../../constants";

const AlbumsList = props => {
    const user = useSelector(state => state.getUser.user);

    const deleteAlbumHandler = async (event) => {
        const token = user.token;
        const headers = {"Authentication": token};
        await deleteAlbum(headers, event.target.value);
    };

    return (
        <div className="album">
            {
                user && user.role==="admin" &&
                <button
                    value={props.id} onClick={deleteAlbumHandler}
                    className="deleteBtn"
                >X</button>
            }
            <div className="albumImage">
                <img src={'http://localhost:'+ port +'/uploads/' + props.image} alt="" className="albumImage"/>
            </div>
            <p className="albumName" id={props.id}>
                <NavLink
                    style={{
                        color: '#000000',
                        textDecoration: 'underline',
                        border: 'none',
                        background: 'none',
                        fontSize: '15px',
                    }}
                    to={`/tracks/${props.id}`}
                >
                {props.name}
                </NavLink>
            </p>
            <p className="albumYear">
                Year: {props.year}
            </p>
        </div>
    );
};

export default AlbumsList;