import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchAlbums} from "../../store/actions";
import AlbumsList from "./AlbumsList";
import RegistrationBlock from "../Registration/RegistrationBlock";
import CreateBlock from "../Create/CreateBlock";

const Albums = props => {
    const dispatch = useDispatch();
    const albums = useSelector(state => state.getAlbums.albums);

    useEffect(() => {
        dispatch(fetchAlbums(props.match.params.id));
    }, [dispatch, props.match.params.id]);

    useEffect(() => {
        const interval = setInterval(async () => {
            dispatch(fetchAlbums(props.match.params.id));
        }, 2000);
        return () => clearInterval(interval);
    }, [dispatch, props.match.params.id]);

    if (albums !== null && albums[0] !== undefined) {
        const albumsList = albums.map((album, index) => {
            return (
                <AlbumsList
                    key={index} name={album.name} image={album.image} id={album._id} year={album.year}
                />
            );
        });
        return (
            <div className="container">
                <header>
                    <h2>Albums</h2>
                    <RegistrationBlock/>
                </header>
                <CreateBlock/>
                <div className="content">
                    <h2>{albums[0].artist.name && albums[0].artist.name}</h2>
                    <div className="albums">
                            {albumsList}
                    </div>
                </div>
            </div>
        );
    } else {
        return (
            <div className="container">
                <header>
                    <h2>Albums</h2>
                    <RegistrationBlock/>
                </header>
                <CreateBlock/>
                <div className="content">
                    <h2>Artist doesn't have any albums</h2>
                </div>
            </div>
        );
    }
};

export default Albums;