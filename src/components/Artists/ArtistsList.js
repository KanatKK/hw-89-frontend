import React from 'react';
import {NavLink} from "react-router-dom";
import {useSelector} from "react-redux";
import {deleteArtist} from "../../store/actions";
import {port} from "../../constants";

const ArtistsList = props => {
    const user = useSelector(state => state.getUser.user);

    const deleteArtistHandler = async (event) => {
        const token = user.token;
        const headers = {"Authentication": token};
        await deleteArtist(headers, event.target.value);
    };

    return (
        <div className="artist">
            {
                user && user.role==="admin" &&
                <button
                    value={props.id} onClick={deleteArtistHandler}
                    className="deleteBtn"
                >X</button>
            }
            <div className="artistImage">
                <img src={'http://localhost:' + port +'/uploads/' + props.image} alt="" className="artistImage"/>
            </div>
            <p className="artistName" id={props.id}>
                <NavLink
                    style={{
                        color: '#000000',
                        textDecoration: 'underline',
                        border: 'none',
                        background: 'none',
                        fontSize: '15px',
                    }}
                    to={`/albums/${props.id}`}
                >
                    {props.name}
                </NavLink>
            </p>
        </div>
    );
};

export default ArtistsList;