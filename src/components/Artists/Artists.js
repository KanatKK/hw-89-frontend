import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchArtists} from "../../store/actions";
import ArtistsList from "./ArtistsList";
import RegistrationBlock from "../Registration/RegistrationBlock";
import CreateBlock from "../Create/CreateBlock";

const Artists = () => {
    const dispatch = useDispatch();
    const artists = useSelector(state => state.getArtists.artists);

    useEffect(() => {
        dispatch(fetchArtists());
    }, [dispatch]);

    useEffect(() => {
        const interval = setInterval(async () => {
            dispatch(fetchArtists());
        }, 2000);
        return () => clearInterval(interval);
    }, [dispatch]);

    if (artists) {
        const artistsList = artists.map((artist, index) => {
            return(
                <ArtistsList
                    key={index} name={artist.name} image={artist.image} id={artist._id}
                />
            );
        });
        return (
            <div className="container">
                <header>
                    <h2>Artists</h2>
                    <RegistrationBlock/>
                </header>
                <CreateBlock/>
                <div className="content">
                    <div className="artists">
                        {artistsList}
                    </div>
                </div>
            </div>
        );
    } else {
        return(
            <div className="container">
                <header>
                    <h2>Artists</h2>
                    <RegistrationBlock/>
                </header>
                <div className="content">
                    <h1>There is no artists. Create them!</h1>
                </div>
            </div>
        );
    }
};

export default Artists;