import React, {useEffect} from 'react';
import {NavLink} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {
    deleteAlbum, deleteArtist, deleteTrack,
    fetchUnpublishedAlbums,
    fetchUnpublishedArtists,
    fetchUnpublishedTracks,
    logOutUser,
    publishAlbum, publishArtist, publishTrack
} from "../../store/actions";

const Unpublished = () => {
    const user = useSelector(state => state.getUser.user);
    const tracks = useSelector(state => state.unpublished.tracks);
    const artists = useSelector(state => state.unpublished.artists);
    const albums = useSelector(state => state.unpublished.albums);
    const dispatch = useDispatch();

    useEffect(() => {
        const fetchUnpublished = async () => {
            const token = user.token;
            const headers = {"Authentication": token};
            await dispatch(fetchUnpublishedTracks(headers));
            await dispatch(fetchUnpublishedArtists(headers));
            await dispatch(fetchUnpublishedAlbums(headers));
        }
        fetchUnpublished().catch(e => console.log(e));
    }, [user.token,dispatch]);

    useEffect(() => {
        const interval = setInterval(async () => {
            const token = user.token;
            const headers = {"Authentication": token};
            await dispatch(fetchUnpublishedTracks(headers));
            await dispatch(fetchUnpublishedArtists(headers));
            await dispatch(fetchUnpublishedAlbums(headers));
        }, 2000);
        return () => clearInterval(interval);
    }, [dispatch, user.token]);

    const logOut = () => {
        dispatch(logOutUser());
    };

    const publishAlbumHandler = async (event) => {
        const token = user.token;
        const headers = {"Authentication": token};
        await publishAlbum(event.target.id, headers);
    };

    const publishArtistHandler = async (event) => {
        const token = user.token;
        const headers = {"Authentication": token};
        await publishArtist(event.target.id, headers);
    };

    const publishTrackHandler = async (event) => {
        const token = user.token;
        const headers = {"Authentication": token};
        await publishTrack(event.target.id, headers);
    };

    const deleteAlbumHandler = async (event) => {
        const token = user.token;
        const headers = {"Authentication": token};
        await deleteAlbum(headers, event.target.value);
    };

    const deleteArtistHandler = async (event) => {
        const token = user.token;
        const headers = {"Authentication": token};
        await deleteArtist(headers, event.target.value);
    };

    const deleteTrackHandler = async (event) => {
        const token = user.token;
        const headers = {"Authentication": token};
        await deleteTrack(headers, event.target.value);
    };

    const unpublishedArtistsList = () => {
        if (artists !== null) {
            return artists.map((artist, index) => {
                return (
                    <div className="artist" key={index}>
                        <div className="artistImage">
                            <img src={'http://localhost:8000/uploads/' + artist.image} alt="" className="artistImage"/>
                        </div>
                        <p className="artistName">
                            {artist.name}
                        </p>
                        <button
                            className="addButton" id={artist._id}
                            onClick={publishArtistHandler} name="publishArtist"
                        >Publish</button>
                        <button
                            className="addButton" value={artist._id}
                            style={{marginTop: 10}} onClick={deleteArtistHandler}
                        >Delete</button>
                    </div>
                );
            });
        }
    };

    const unpublishedAlbumsList = () => {
        if (albums !== null) {
            return albums.map((album, index) => {
                return (
                    <div className="album" key={index}>
                        <div className="albumImage">
                            <img src={'http://localhost:8000/uploads/' + album.image} alt="" className="albumImage"/>
                        </div>
                        <p className="albumName">
                            {album.name}
                        </p>
                        <p className="albumYear">
                            Year: {album.year}
                        </p>
                        <button
                            className="addButton" id={album._id}
                            onClick={publishAlbumHandler} name="publishAlbum"
                        >Publish</button>
                        <button
                            className="addButton" value={album._id}
                            style={{marginTop: 10}} onClick={deleteAlbumHandler}
                        >Delete</button>
                    </div>
                );
            });
        }
    };

    const unpublishedTrackList = () => {
        if (tracks !== null) {
            return tracks.map((track,index) => {
                return (
                    <div className="track" key={index}>
                        <div className="trackNumber">
                            {track.number}
                        </div>
                        <div className="trackInfo">
                            <h4>{track.name}</h4>
                            <p>{track.duration}</p>
                        </div>
                        <div className="player">
                            <button
                                className="addButton" id={track._id}
                                onClick={publishTrackHandler} name="publishTrack"
                            >Publish</button>
                            <button
                                className="addButton" value={track._id}
                                style={{marginTop: 10}} onClick={deleteTrackHandler}
                            >Delete</button>
                        </div>
                    </div>
                );
            });
        }
    };

    return (
        <div className="container">
            <header>
                <h2>Unpublished</h2>
                <span className="nav">
                <span className="trackHistory">
                    <NavLink
                        style={{
                            color: 'black',
                            textDecoration: 'underline',
                            cursor: 'pointer'
                        }}
                        to={'/track_history/' + user.username}
                    >
                        Track History
                    </NavLink>
                </span>
                <span className="name" onClick={logOut}>
                    <NavLink
                        style={{
                            color: 'black',
                            textDecoration: 'underline',
                            cursor: 'pointer'
                        }}
                        to="/"
                    >{user.username}</NavLink>
                </span>
            </span>
            </header>
            <div className="content">
                <div className="unpublishedArtists">
                    <h3>Unpublished artists:</h3>
                    <div className="artists">
                        {
                            artists &&
                            unpublishedArtistsList()
                        }
                        {
                            !artists && <p>There are no unpublished Artists!</p>
                        }
                    </div>
                </div>
                <div className="unpublishedAlbums">
                    <h3>Unpublished albums:</h3>
                    <div className="albums">
                        {
                            albums &&
                            unpublishedAlbumsList()
                        }
                        {
                            !albums && <p>There are no unpublished Albums!</p>
                        }
                    </div>
                </div>
                <div className="unpublishedTracks">
                    <h3>Unpublished tracks:</h3>
                    <div className="tracks">
                        {
                            tracks &&
                            unpublishedTrackList()
                        }
                        {
                            !tracks && <p>There are no unpublished Tracks</p>
                        }
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Unpublished;