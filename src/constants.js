export let port = 8000;
if (process.env.REACT_APP_NODE_ENV  === "test") {
    port = 8010;
}
export const apiUrl = "http://localhost:" + port;
export const fbAppId = "861692451283488";